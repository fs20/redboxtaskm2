<?php
namespace RedboxDigital\Linkedin\Setup;

use Magento\Customer\Setup\CustomerSetup;
use Magento\Customer\Model\Customer;

class InstallData implements \Magento\Framework\Setup\InstallDataInterface
{
    private $_customerSetupFactory;

    /**
     * Initializing required params
     *
     * @param \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory
     */
    public function __construct(\Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory) {
        $this->_customerSetupFactory = $customerSetupFactory;
    }

    /**
     * Installing data
     *
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $setup
     * @param \Magento\Framework\Setup\ModuleContextInterface $context
     */
    public function install(
        \Magento\Framework\Setup\ModuleDataSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        /** @var CustomerSetup $customerSetup */
        $customerSetup = $this->_customerSetupFactory->create(['setup' => $setup]);
        $setup->startSetup();
        $customerSetup->removeAttribute('customer', 'linkedin_profile');
        $customerSetup->addAttribute('customer', 'linkedin_profile', [
            'label' => 'Linkedin Profile',
            'type' => 'varchar',
            'input' => 'text',
            'position' => 200,
            'visible' => true,
            'required' => false,
            'unique' => true,
        ]);

        $linkedInProfile = $customerSetup->getEavConfig()->getAttribute('customer', 'linkedin_profile');
        $linkedInProfile->setData('used_in_forms', [
            'adminhtml_customer',
            'adminhtml_checkout',
            'customer_account_edit',
            'customer_account_create',
            'checkout_register',
        ])
            ->setData("is_used_for_customer_segment", true)
            ->setData("is_system", 0)
            ->setData("is_user_defined", 1)
            ->setData("is_visible", 1)
            ->setData("sort_order", 100)->save();

        $setup->endSetup();
    }
}