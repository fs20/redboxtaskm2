<?php
namespace RedboxDigital\Linkedin\Block\Widget;

use Magento\Customer\Block\Widget\AbstractWidget;

class Profile extends AbstractWidget
{
    const ATTRIBUTE_CODE = 'linkedin_profile';
    /**
     * Initializing linkedin profile template
     *
     * @return void
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('widget/profile.phtml');
    }

    /**
     * Check if company attribute enabled in system
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->_getAttribute(self::ATTRIBUTE_CODE) ?
            (bool) $this->_getAttribute(self::ATTRIBUTE_CODE)->isVisible() : false;
    }


    /**
     * Check if company attribute marked as required
     *
     * @return bool
     */
    public function isRequired()
    {
        return $this->_getAttribute(self::ATTRIBUTE_CODE) ? (bool)$this->_getAttribute(self::ATTRIBUTE_CODE)
            ->isRequired() : false;
    }
}