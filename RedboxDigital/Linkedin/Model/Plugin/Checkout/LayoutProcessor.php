<?php
namespace RedboxDigital\Linkedin\Model\Plugin\Checkout;

class LayoutProcessor
{

    protected $_linkedInMetaData = null;

    /**
     * After processing layout
     *
     * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        array  $jsLayout
    ) {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        /** @var \Magento\Customer\Model\Metadata\CustomerMetadata $customerMetadata */
        $customerMetadata = $objectManager->create('Magento\Customer\Model\Metadata\CustomerMetadata');
        $this->_linkedInMetaData = $customerMetadata->getAttributeMetadata('linkedin_profile');
        $jsLayout = $this->_processShippingAddress($jsLayout);
        return $this->_processBillingAddress($jsLayout);
    }

    /**
     * Process Billing address
     *
     * @param array $jsLayout
     * @return array
     */
    protected function _processBillingAddress($jsLayout)
    {
        $configuration = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']
        ['children']['payment']['children']['payments-list']['children'];
        foreach ($configuration as $paymentGroup => $groupConfig) {
            if (isset($groupConfig['component']) AND $groupConfig['component'] === 'Magento_Checkout/js/view/billing-address') {
                $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                ['payment']['children']['payments-list']['children'][$paymentGroup]['children']['form-fields']['children']
                ['linkedin_profile'] = [
                    'component' => 'Magento_Ui/js/form/element/abstract',
                    'config' => [
                        'customScope' => 'billingAddress.custom_attributes',
                        'template' => 'ui/form/field',
                        'elementTmpl' => 'ui/form/element/input',
                        'options' => [],
                        'id' => 'billing-linkedin-profile'
                    ],
                    'dataScope' => 'billingAddress.custom_attributes.linkedin_profile',
                    'label' => 'LinkedIn Profile',
                    'provider' => 'checkoutProvider',
                    'visible' => $this->_linkedInMetaData->isVisible(),
                    'validation' => [
                        'required-entry' => $this->_linkedInMetaData->isRequired(),
                    ],
                    'sortOrder' => 250,
                    'id' => 'billing-linkedin-profile'
                ];
            }
        }
        return $jsLayout;
    }

    /**
     * Processing shipping address
     *
     * @param array $jsLayout
     * @return array
     */
    protected function _processShippingAddress($jsLayout)
    {
        $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']['children']
        ['shippingAddress']['children']['shipping-address-fieldset']['children']['linkedin_profile'] = [
            'component' => 'Magento_Ui/js/form/element/abstract',
            'config' => [
                'customScope' => 'shippingAddress.custom_attributes',
                'template' => 'ui/form/field',
                'elementTmpl' => 'ui/form/element/input',
                'options' => ['class' => 'required-entry'],
                'id' => 'shipping-linkedin-profile'
            ],
            'dataScope' => 'shippingAddress.custom_attributes.linkedin_profile',
            'label' => 'Linkedin Profile',
            'provider' => 'checkoutProvider',
            'visible' => $this->_linkedInMetaData->isVisible(),
            'validation' => [
                'required-entry' => $this->_linkedInMetaData->isRequired(),
            ],
            'sortOrder' => 250,
            'id' => 'shipping-linkedin-profile'
        ];
        return $jsLayout;
    }
}